# Welcome to the OC Issue Tracker!

Here, you'll be able to submit bug reports, new features, or QOL improvements you would like to see in one of our bots.

## Cool, how do I get started?

Choose a template from one of the respectively named files on this repo, then create a new issue, pasting that template into your issue then filling out the information.

## How do I know if my suggestion was good?

We will try to reply to each one of our Issue Tracker issues, giving proper feedback, and of course ensuring whether or not your submission was approved or denied.

# We will deny any issue not following one of our templates.