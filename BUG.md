# Bug Template

## Which bot are you reporting for?

## Which part of the bot does this apply to? (the category of commands, ex. moderation, economy)

## What steps did you take to reproduce this issue?

## Please be descriptive as possible, and write your complete bug report.
